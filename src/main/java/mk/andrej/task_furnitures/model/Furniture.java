package mk.andrej.task_furnitures.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class Furniture {
	
	private String type;
	private @NonNull String wood;
	private @NonNull String glass;
	private @NonNull String plastic;

}
