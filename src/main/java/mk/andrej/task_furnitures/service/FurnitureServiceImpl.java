package mk.andrej.task_furnitures.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;

import mk.andrej.task_furnitures.model.Furniture;

@Service
public class FurnitureServiceImpl implements FurnitureService {
	private final Integer woodKg = 9;
	private final Integer glassKg = 11;
	private final Integer plasticKg = 4;

	public String getJsonAsString(String json) {
		JSONObject obj = new JSONObject(json);
		
		
		return obj.toString();
		
		
		/*String type = obj.
		//JSONArray furnitureArray = obj.getJSONArray("furnitures");

		//String type = furnitureArray.getString(1);

		if (type == "cupboard") {
			return countCupboard(obj.getInt("wood"));
		} else if (type == "chair") {
			return countChair(obj.getInt("wood"), obj.getInt("glass"));
		} else if (type == "table") {
			return countTeaTable(obj.getInt("wood"), obj.getInt("glass"), obj.getInt("plastic"));

		}
		return 0;
*/
	}

	public Integer countCupboard(Integer wood) {
		return wood * woodKg;

	}

	public Integer countChair(Integer wood, Integer glass) {
		return wood * woodKg + glass * glassKg;

	}

	public Integer countTeaTable(Integer wood, Integer glass, Integer plastic) {
		return wood * woodKg + glass * glassKg + plastic * plasticKg;

	}

}
