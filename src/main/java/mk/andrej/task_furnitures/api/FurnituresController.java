package mk.andrej.task_furnitures.api;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mk.andrej.task_furnitures.service.FurnitureServiceImpl;

@RestController
@RequestMapping("/furniture")
public class FurnituresController {

	@Autowired
	private FurnitureServiceImpl furnitureService;

	@PostMapping(value = "/add", consumes = { "application/json" })
	public String calculateMostExpensive(@RequestBody String json) {
		return furnitureService.getJsonAsString(json);

	}

}
