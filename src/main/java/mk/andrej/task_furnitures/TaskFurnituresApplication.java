package mk.andrej.task_furnitures;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskFurnituresApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskFurnituresApplication.class, args);
	}

}
